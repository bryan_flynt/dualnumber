# ****************************************************************************
# * Makefile to build the test program for the fortran dual numbers.         *
# ****************************************************************************

# **********************************
# *         Definitions.           *
# **********************************

MAKE       = gmake
FF90       = gfortran
FF90_FLAGS = -O3
#FF90_FLAGS = -O3 -fdefault-real-8

# **********************************
# *         Sources.               *
# **********************************

FF90_SRC = ModDualNumber.f90\
	   MyRoutineDual.f90\
	   MyRoutine.f90\
	   TestProgram.f90

# **********************************
# *         Objects.               *
# **********************************

FF90_OBJECTS = $(FF90_SRC:%.f90=%.o)

# ****************************************************************************
# * Rules to make the objects. Redefine .SUFFIXES to be sure all the desired *
# * ones are included.                                                       *
# ****************************************************************************

.SUFFIXES: .o .f .F .f90 .F90 .c .C .cc .cpp .h .hh .H

.f90.o:
	$(FF90) $(FF90_FLAGS) -c $<
	@echo
	@echo "        --- Compiled $*.f90 successfully ---"
	@echo

# **********************************
# *         Targets.               *
# **********************************

default: all

all:	TestProgram

clean:
	@echo " ---->  Making clean ..."
	rm -f *~ *.mod *.o

clean_all:
	@echo " ---->  Making clean_all ..."
	rm -f *~ *.mod *.o TestProgram

TestProgram: $(FF90_OBJECTS) Makefile
	@echo " ---->  Creating TestProgram ..."
	@echo
	$(FF90) $(FF90_OBJECTS) $(FF90__FLAGS) -o TestProgram
	@echo
	@echo " ---->  TestProgram created !!!"
	@echo

# ****************************************************************************
# *                              Dependencies.                               *
# ****************************************************************************

ModuleDualNumber.o: Makefile
MyRoutineDual.o:    Makefile ModDualNumber.f90
MyRoutine.o:        Makefile
TestProgram.o:      Makefile ModDualNumber.f90
