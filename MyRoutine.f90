subroutine MyRoutine(a, b, output)
!
!******************************************************************
!* Subroutine, which computes the output as a function of the     *
!* input a & b.                                                   *
!******************************************************************
!
  implicit none
  real, intent(in)  :: a
  real, intent(in)  :: b
  real, intent(out) :: output
  
  ! Something kind of complex to calculate
  output = 3.0 * a**2 + sin(b) - log10(a**1.5)
  
end subroutine MyRoutine
