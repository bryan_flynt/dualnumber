# README #

Fortran DualNumber Implentation

### What is this repository for? ###

Simple test program to show how to use the DualNumber data type to calculate derivatives 

### How do I get set up? ###

The DualNumber data type is defined within ModDualNumber.f90 and this is the only piece 
you need to include within your own project.  The rest of the routines are the test program 
to show how to use the derived data type. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact