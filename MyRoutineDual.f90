subroutine MyRoutineDual(a, b, output)
!
!  ******************************************************************
!  * Subroutine, which computes the output as a function of the     *
!  * input a & b.                                                   *
!  ******************************************************************
!
  use ModDualNumber
  implicit none
  
  type (DualNumber), intent(in)  :: a
  real, intent(in)  :: b
  type (DualNumber), intent(out) :: output
  
  ! Something kind of complex to calculate
  output = 3.0 * a**2 + sin(b) - log10(a**1.5)
  
end subroutine MyRoutineDual
