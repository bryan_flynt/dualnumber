      program TestProgram
      use ModDualNumber
      implicit none

      ! An extra real value
      real :: extra

      ! Finite Difference Variables
      real :: a, b, da, db
      real, parameter :: eta = 1.0e-5

      ! Dual Number Variables
      type (DualNumber) :: aDual, bDual

      !**********************************************************
      !                     Test Program
      !**********************************************************

      ! Assign a number to a variable which is used in the
      ! calculation but is neither the result nor variable
      ! we want to get the derivative with respect to.
      extra = 0.12345
      
      !----- Use Finite Difference to get Gradient -----

      ! Calulate the standard routine values
      a = 3.0
      call MyRoutine(a,extra,b)

      ! Calculate the perturbed value
      da = a + eta
      call MyRoutine(da,extra,db)

      ! Report Results
      write(*,*)
      write(*,*) 'Finite Difference Results:'
      write(*,*) 'Routine Result    = ', b
      write(*,*) 'Derivative Result = ', (db-b)/eta

      !----- Use Dual Number to get Gradient -----

      ! Assign value & perturbation to aDual
      aDual%val = a
      aDual%der = 1.0
      call MyRoutineDual(aDual,extra,bDual)

      ! Report Results
      write(*,*)
      write(*,*) 'DualNumber Results:'
      write(*,*) 'Routine Result    = ', bDual%val
      write(*,*) 'Derivative Result = ', bDual%der

      ! Report Exact
      write(*,*)
      write(*,*) 'Exact Results:'
      write(*,*) 'Routine Result    = ', b
      write(*,*) 'Derivative Result = ', 6.0*a - 0.651442/a
      
      end program TestProgram
