      module ModDualNumber
!
!     ******************************************************************
!     *                                                                *
!     * Module that contains the definition of the dual number type    *
!     * and its operators and intrinsic functions.                     *
!     *                                                                *
!     ******************************************************************
!
      implicit none
!
      !------------------------------------
      ! Definition of the dual number type.
      !------------------------------------

      type DualNumber
        sequence

        ! Real part of the dual number.
        real :: val

        ! Eps part of the dual number.
        real :: der
      end type DualNumber

      !-----------
      ! Operators.
      !-----------

      ! Equal assignment
      interface assignment (=)
        module procedure EqualDualDual
        module procedure EqualDualReal
      end interface

      ! Unary operator +
      interface operator (+)
        module procedure PlusDualDual
      end interface

      ! Addition operator
      interface operator (+)
        module procedure AddDualDual
        module procedure AddDualReal
        module procedure AddRealDual
      end interface

      ! Unary operator -
      interface operator (-)
        module procedure MinusDualDual
      end interface

      ! Subtraction operator
      interface operator (-)
        module procedure SubtractDualDual
        module procedure SubtractDualReal
        module procedure SubtractRealDual
      end interface

      ! Multiplication operator
      interface operator (*)
        module procedure MultiplyDualDual
        module procedure MultiplyDualReal
        module procedure MultiplyRealDual
        module procedure MultiplyDualInt
        module procedure MultiplyIntDual
      end interface

      ! Division operator
      interface operator (/)
        module procedure DivideDualDual
        module procedure DivideDualReal
        module procedure DivideRealDual
      end interface

      ! Power operator
      interface operator (**)
        module procedure PowerDualInt
        module procedure PowerDualDual
        module procedure PowerDualReal
      end interface

      !-------------------
      ! Logical operators.
      !-------------------

      ! Equal operator.
      interface operator (.eq.)  ! or (==)
        module procedure eq_dd
        module procedure eq_dr
        module procedure eq_rd
        module procedure eq_di
        module procedure eq_id
      end interface

      ! Not equal operator.
      interface operator (.ne.)  ! or (/=)
        module procedure ne_dd
        module procedure ne_dr
        module procedure ne_rd
        module procedure ne_di
        module procedure ne_id
      end interface

      ! Less than operator.
      interface operator (.lt.)  ! or (<)
        module procedure lt_dd
        module procedure lt_dr
        module procedure lt_rd
        module procedure lt_di
        module procedure lt_id
      end interface

      ! Less than or equal operator.
      interface operator (.le.)  ! or (<=)
        module procedure le_dd
        module procedure le_dr
        module procedure le_rd
        module procedure le_di
        module procedure le_id
      end interface

      ! Greater than operator.
      interface operator (.gt.)  ! or (>)
        module procedure gt_dd
        module procedure gt_dr
        module procedure gt_rd
        module procedure gt_di
        module procedure gt_id
      end interface

      ! Greater than or equal operator.
      interface operator (.ge.)  ! or (>=)
        module procedure ge_dd
        module procedure ge_dr
        module procedure ge_rd
        module procedure ge_di
        module procedure ge_id
      end interface

      !----------------
      ! Math functions.
      !----------------

      ! Absolute value function
      interface abs
        module procedure absDual
      end interface

      ! Integer function
      interface int
        module procedure intDual
      end interface

      ! Nearest integer function
      interface nint
        module procedure nintDual
      end interface

      ! Real function
      interface real
        module procedure realDual
      end interface

      ! Sign function
      interface sign
        module procedure sign_dd
        module procedure sign_dr
        module procedure sign_rd
      end interface

      ! Sine function
      interface sin
        module procedure sinDual
      end interface

      ! Cosine function
      interface cos
        module procedure cosDual
      end interface

      ! Tangent function
      interface tan
        module procedure tanDual
      end interface

      ! Sqrt function
      interface sqrt
        module procedure sqrtDual
      end interface

      ! Log function
      interface log
        module procedure logDual
      end interface

      ! Log10 function
      interface log10
        module procedure log10Dual
      end interface

      ! Exp function
      interface exp
        module procedure expDual
      end interface

      ! Sinh function
      interface sinh
        module procedure sinhDual
      end interface

      ! Cosh function
      interface cosh
        module procedure coshDual
      end interface

      ! Tanh function
      interface tanh
        module procedure tanhDual
      end interface

      ! Acos function
      interface acos
        module procedure acosDual
      end interface

      ! Asin function
      interface asin
        module procedure asinDual
      end interface

      ! Atan function
      interface atan
        module procedure atanDual
      end interface

      ! Atan2 function
      interface atan2
        module procedure atan2Dual
      end interface

      ! Max function (limited to combinations below, but that
      ! can be extended)
      interface max
        module procedure max_dd
        module procedure max_ddd
        module procedure max_dr
        module procedure max_rd
      end interface

      ! Min function (limited for now to 2 arguments, but that
      ! can be extended)
      interface min
        module procedure min_dd
        module procedure min_dr
        module procedure min_rd
      end interface

      !=================================================================

      contains

        !===============================================================

        !------------------------------------
        ! Functions for the equal assignment.
        !------------------------------------

        subroutine EqualDualDual(res, inp)
        implicit none
        type (DualNumber), intent(out) :: res
        type (DualNumber), intent(in)  :: inp

        res%val = inp%val
        res%der = inp%der
        end subroutine EqualDualDual

        subroutine EqualDualReal(res, inp)
        implicit none
        type (DualNumber), intent(out) :: res
        real,              intent(in)  :: inp

        res%val = inp
        res%der = 0.0
        end subroutine EqualDualReal

        !-----------------------------------
        ! Function for the unary operator +.
        !-----------------------------------

        function PlusDualDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val = v1%val
        v2%der = v1%der
        end function PlusDualDual

        !-------------------------------------
        ! Functions for the addition operator.
        !-------------------------------------

        function AddDualDual(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1,v2
        type (DualNumber) :: v3

        v3%val = v1%val + v2%val
        v3%der = v1%der + v2%der
        end function AddDualDual

        function AddDualReal(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real, intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1%val + v2
        v3%der = v1%der
        end function AddDualReal

        function AddRealDual(v1,v2) result (v3)
        real, intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1 + v2%val
        v3%der =      v2%der
        end function AddRealDual

        !-----------------------------------
        ! Function for the unary operator -.
        !-----------------------------------

        function MinusDualDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val = -v1%val
        v2%der = -v1%der
        end function MinusDualDual

        !----------------------------------------
        ! Functions for the subtraction operator.
        !----------------------------------------

        function SubtractDualDual(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1,v2
        type (DualNumber) :: v3

        v3%val = v1%val - v2%val
        v3%der = v1%der - v2%der
        end function SubtractDualDual

        function SubtractDualReal(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real, intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1%val - v2
        v3%der = v1%der
        end function SubtractDualReal

        function SubtractRealDual(v1,v2) result (v3)
        real, intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1 - v2%val
        v3%der =    - v2%der
        end function SubtractRealDual

        !-------------------------------------------
        ! Functions for the multiplication operator.
        !-------------------------------------------

        function MultiplyDualDual(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1,v2
        type (DualNumber) :: v3

        v3%val = v1%val * v2%val
        v3%der = v1%val * v2%der + v1%der * v2%val
        end function MultiplyDualDual

        function MultiplyDualReal(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real, intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1%val * v2
        v3%der = v1%der * v2
        end function MultiplyDualReal

        function MultiplyRealDual(v1,v2) result (v3)
        real, intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1 * v2%val
        v3%der = v1 * v2%der
        end function MultiplyRealDual

        function MultiplyDualInt(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        integer, intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1%val * v2
        v3%der = v1%der * v2
        end function MultiplyDualInt

        function MultiplyIntDual(v1,v2) result (v3)
        integer, intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3

        v3%val = v1 * v2%val
        v3%der = v1 * v2%der
        end function MultiplyIntDual

        !-------------------------------------
        ! Functions for the division operator.
        !-------------------------------------

        function DivideDualDual(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1,v2
        type (DualNumber) :: invV2, v3

        invV2%val =  1.0/v2%val
        invV2%der = -invV2%val * invV2%val * v2%der
        v3%val = v1%val * invV2%val
        v3%der = v1%val * invV2%der + v1%der * invV2%val
        end function DivideDualDual

        function DivideDualReal(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real, intent(in) :: v2
        type (DualNumber) :: v3
        real :: invV2

        invV2 = 1.0/v2
        v3%val = v1%val * invV2
        v3%der = v1%der * invV2
        end function DivideDualReal

        function DivideRealDual(v1,v2) result (v3)
        real, intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: invV2, v3

        invV2%val =  1.0/v2%val
        invV2%der = -invV2%val * invV2%val * v2%der
        v3%val = v1 * invV2%val
        v3%der = v1 * invV2%der
        end function DivideRealDual

        !-------------------------------------
        ! Functions for the power operator.
        !-------------------------------------

        function PowerDualInt(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        integer,           intent(in) :: v2
        integer :: i, vv2
        type (DualNumber) :: v3

        v3  = 1.0
        vv2 = abs(v2)
        do i=1,vv2
          v3 = v3*v1
        enddo

        if(v2 < 0) v3 = 1.0/v3
        end function PowerDualInt

        function PowerDualDual(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1, v2
        type (DualNumber) :: v3, v4

        v4 = logDual(v1)
        v3 = expDual(v2*v4)
        end function PowerDualDual

        function PowerDualReal(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real,              intent(in) :: v2
        type (DualNumber) :: v3

        real, parameter :: tol = 1.e-15
        real :: xval, deriv

        xval = v1%val
        if(abs(xval) < tol) then
          if(xval >= 0.0) then
            xval = tol
          else
            xval = -tol
          endif
        endif

        deriv = v2*(xval**(v2-1.0))
        v3%val = (v1%val)**v2
        v3%der =  v1%der *deriv
        end function PowerDualReal

        !----------------------------------
        ! Functions for the equal operator.
        !----------------------------------

        logical function eq_dd(lhs, rhs)
          type (DualNumber), intent(in) :: lhs, rhs
          eq_dd = lhs%val == rhs%val
        end function eq_dd

        logical function eq_dr(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          real, intent(in) :: rhs
          eq_dr = lhs%val == rhs
        end function eq_dr

        logical function eq_rd(lhs, rhs)
          real, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          eq_rd = lhs == rhs%val
        end function eq_rd

        logical function eq_di(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          integer, intent(in) :: rhs
          eq_di = lhs%val == rhs
        end function eq_di

        logical function eq_id(lhs, rhs)
          integer, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          eq_id = lhs == rhs%val
        end function eq_id

        !--------------------------------------
        ! Functions for the not equal operator.
        !--------------------------------------

        logical function ne_dd(lhs, rhs)
          type (DualNumber), intent(in) :: lhs, rhs
          ne_dd = lhs%val /= rhs%val
        end function ne_dd

        logical function ne_dr(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          real, intent(in) :: rhs
          ne_dr = lhs%val /= rhs
        end function ne_dr

        logical function ne_rd(lhs, rhs)
          real, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          ne_rd = lhs /= rhs%val
        end function ne_rd

        logical function ne_di(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          integer, intent(in) :: rhs
          ne_di = lhs%val /= rhs
        end function ne_di

        logical function ne_id(lhs, rhs)
          integer, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          ne_id = lhs /= rhs%val
        end function ne_id

        !--------------------------------------
        ! Functions for the less than operator.
        !--------------------------------------

        logical function lt_dd(lhs, rhs)
          type (DualNumber), intent(in) :: lhs, rhs
          lt_dd = lhs%val < rhs%val
        end function lt_dd

        logical function lt_dr(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          real, intent(in) :: rhs
          lt_dr = lhs%val < rhs
        end function lt_dr

        logical function lt_rd(lhs, rhs)
          real, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          lt_rd = lhs < rhs%val
        end function lt_rd

        logical function lt_di(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          integer, intent(in) :: rhs
          lt_di = lhs%val < rhs
        end function lt_di

        logical function lt_id(lhs, rhs)
          integer, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          lt_id = lhs < rhs%val
        end function lt_id

        !-----------------------------------------------
        ! Functions for the less than or equal operator.
        !-----------------------------------------------

        logical function le_dd(lhs, rhs)
          type (DualNumber), intent(in) :: lhs, rhs
          le_dd = lhs%val <= rhs%val
        end function le_dd

        logical function le_dr(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          real, intent(in) :: rhs
          le_dr = lhs%val <= rhs
        end function le_dr

        logical function le_rd(lhs, rhs)
          real, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          le_rd = lhs <= rhs%val
        end function le_rd

        logical function le_di(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          integer, intent(in) :: rhs
          le_di = lhs%val <= rhs
        end function le_di

        logical function le_id(lhs, rhs)
          integer, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          le_id = lhs <= rhs%val
        end function le_id

        !-----------------------------------------
        ! Functions for the greater than operator.
        !-----------------------------------------

        logical function gt_dd(lhs, rhs)
          type (DualNumber), intent(in) :: lhs, rhs
          gt_dd = lhs%val > rhs%val
        end function gt_dd

        logical function gt_dr(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          real, intent(in) :: rhs
          gt_dr = lhs%val > rhs
        end function gt_dr

        logical function gt_rd(lhs, rhs)
          real, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          gt_rd = lhs > rhs%val
        end function gt_rd

        logical function gt_di(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          integer, intent(in) :: rhs
          gt_di = lhs%val > rhs
        end function gt_di

        logical function gt_id(lhs, rhs)
          integer, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          gt_id = lhs > rhs%val
        end function gt_id

        !--------------------------------------------------
        ! Functions for the greater than or equal operator.
        !--------------------------------------------------
        
        logical function ge_dd(lhs, rhs)
          type (DualNumber), intent(in) :: lhs, rhs
          ge_dd = lhs%val >= rhs%val
        end function ge_dd

        logical function ge_dr(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          real, intent(in) :: rhs
          ge_dr = lhs%val >= rhs
        end function ge_dr

        logical function ge_rd(lhs, rhs)
          real, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          ge_rd = lhs >= rhs%val
        end function ge_rd

        logical function ge_di(lhs, rhs)
          type (DualNumber), intent(in) :: lhs
          integer, intent(in) :: rhs
          ge_di = lhs%val >= rhs
        end function ge_di

        logical function ge_id(lhs, rhs)
          integer, intent(in) :: lhs
          type (DualNumber), intent(in) :: rhs
          ge_id = lhs >= rhs%val
        end function ge_id

        !----------------
        ! Math functions.
        !----------------

        ! Absolute value function.
        function absDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        if(v1%val >= 0.0) then
          v2%val = v1%val
          v2%der = v1%der
        else
          v2%val = -v1%val
          v2%der = -v1%der
        endif
        end function absDual

        ! Integer function.
        function intDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        integer :: v2

        v2 = int(v1%val)
        end function intDual

        ! Nearest integer function.
        function nintDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        integer :: v2

        v2 = nint(v1%val)
        end function nintDual

        ! Real function.
        function realDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        real :: v2

        v2 = v1%val
        end function realDual

        ! Functions for the sign function.
        function sign_dd(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1, v2
        type (DualNumber) :: v3
        real :: ssign

        if(v2%val < 0.0) then
          ssign = -1.0
        else
          ssign =  1.0
        endif
        v3 = ssign*v1
        end function sign_dd

        function sign_dr(v1,v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real,              intent(in) :: v2
        type (DualNumber) :: v3
        real :: ssign

        if(v2 < 0.0) then
          ssign = -1.0
        else
          ssign =  1.0
        endif
        v3 = ssign*v1
        end function sign_dr

        function sign_rd(v1,v2) result (v3)
        real,              intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3
        real :: ssign

        if(v2%val < 0.0) then
          ssign = -1.0
        else
          ssign =  1.0
        endif
        v3 = ssign*v1
        end function sign_rd

        ! Sine function.
        function sinDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =         sin(v1%val)
        v2%der = v1%der * cos(v1%val)
        end function sinDual

        ! Cosine function.
        function cosDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =          cos(v1%val)
        v2%der = -v1%der * sin(v1%val)
        end function cosDual

        ! Tangent function.
        function tanDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =          tan(v1%val)
        v2%der = -v1%der * (v2%val*v2%val + 1.0)
        end function tanDual

        ! Sqrt function
        function sqrtDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =        sqrt(v1%val)
        v2%der = v1%der/(2*v2%val)
        end function sqrtDual

        ! Log function
        function logDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =       log(v1%val)
        v2%der = v1%der/v1%val
        end function logDual

        ! Log10 function
        function log10Dual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =       log10(v1%val)
        v2%der = v1%der/(log(10.0)*v1%val)
        end function log10Dual

        ! Exp function
        function expDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2

        v2%val =         exp(v1%val)
        v2%der = v1%der * v2%val
        end function expDual

        ! Sinh function
        function sinhDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: t1, t2, v2

        t1 = exp(v1)
        t2 = exp(-v1)
        v2 = 0.5*(t1-t2)
        end function sinhDual

        ! Cosh function
        function coshDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: t1, t2, v2
        
        t1 = exp(v1)
        t2 = exp(-v1)
        v2 = 0.5*(t1+t2)
        end function coshDual

        ! Tanh function
        function tanhDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: t1, t2, v2
        
        t1 = exp(v1)
        t2 = exp(-v1)
        v2 = (t1-t2)/(t1+t2)
        end function tanhDual

        ! Acos function
        function acosDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2
        real              :: deriv

        deriv = -1.0/sqrt(1.0 - v1%val*v1%val)
        v2%val = acos(v1%val)
        v2%der = deriv*v1%der
        end function acosDual

        ! Asin function
        function asinDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2
        real              :: deriv

        deriv = 1.0/sqrt(1.0 - v1%val*v1%val)
        v2%val = asin(v1%val)
        v2%der = deriv*v1%der
        end function asinDual

        ! Atan function
        function atanDual(v1) result (v2)
        type (DualNumber), intent(in) :: v1
        type (DualNumber) :: v2
        real              :: deriv

        deriv = 1.0/(1.0 + v1%val*v1%val)
        v2%val = atan(v1%val)
        v2%der = deriv*v1%der
        end function atanDual

        ! Atan2 function
        function atan2Dual(v1, v2) result (v3)
        type (DualNumber), intent(in) :: v1, v2
        type (DualNumber) :: v3
        real              :: a, b, c, d

        a = v1%val; b = v1%der
        c = v2%val; d = v2%der

        v3%val = atan2(a,c)
        v3%der = (c*b - a*d)/(a*a + c*c)
        end function atan2Dual

        ! Max functions
        function max_dd(v1, v2) result (v3)
        type (DualNumber), intent(in) :: v1, v2
        type (DualNumber) :: v3

        if(v1%val > v2%val) then
          v3 = v1
        else
          v3 = v2
        endif
        end function max_dd

        function max_ddd(v1, v2, v3) result (v4)
        type (DualNumber), intent(in) :: v1, v2, v3
        type (DualNumber) :: v4

        if(v1%val > v2%val) then
          v4 = v1
        else
          v4 = v2
        endif

        if(v3%val > v4%val) v4 = v3
        end function max_ddd

        function max_dr(v1, v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real,              intent(in) :: v2
        type (DualNumber) :: v3

        if(v1%val > v2) then
          v3 = v1
        else
          v3 = v2
        endif
        end function max_dr

        function max_rd(v1, v2) result (v3)
        real,              intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3

        if(v1 > v2%val) then
          v3 = v1
        else
          v3 = v2
        endif
        end function max_rd

        ! Min functions
        function min_dd(v1, v2) result (v3)
        type (DualNumber), intent(in) :: v1, v2
        type (DualNumber) :: v3

        if(v1%val < v2%val) then
          v3 = v1
        else
          v3 = v2
        endif
        end function min_dd

        function min_dr(v1, v2) result (v3)
        type (DualNumber), intent(in) :: v1
        real,              intent(in) :: v2
        type (DualNumber) :: v3

        if(v1%val < v2) then
          v3 = v1
        else
          v3 = v2
        endif
        end function min_dr

        function min_rd(v1, v2) result (v3)
        real,              intent(in) :: v1
        type (DualNumber), intent(in) :: v2
        type (DualNumber) :: v3

        if(v1 < v2%val) then
          v3 = v1
        else
          v3 = v2
        endif
        end function min_rd

      end module ModDualNumber
